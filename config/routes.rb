Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users
  post '/programmes/:id'    => 'registrations#register'
  delete '/programmes/:id'  => 'registrations#delete'

  resources :programmes do
    resources :sponsors
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'programmes#home'

  # Example of regular route:
  get '/result' => 'programmes#result'
  get '/about' => 'programmes#about'
  get '/profile' => 'profile#show'
  get '/credit' => 'programmes#credit'
  get '/schedule' => 'programmes#schedule'


  get 'api/programmes/events' => 'programmes#api_events'
  get 'api/programmes/category/:category' => 'programmes#api_category'
  get 'api/programmes/sponsors' => 'programmes#api_sponsors'
  get 'api/programmes/:id' => 'programmes#api_id'
  get 'push/android' => 'android#create'
  get 'api/push/add' => 'android#new'
  get 'info' => 'sessions#info'
  get 'auth/:provider/callback', to: 'sessions#create'
  post "users/create", to: 'sessions#add_user'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

end
