OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
	provider :facebook, "549046171928982", "8a52b4ddaf9fb2305b59d1240a91b407",
	scope: 'email', info_fields: 'uid,name,email,image,token,expires_at'
end

OmniAuth.config.on_failure = Proc.new do |env|
  SessionsController.action(:auth_failure).call(env)
end