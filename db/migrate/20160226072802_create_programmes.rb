class CreateProgrammes < ActiveRecord::Migration
  def change
    create_table :programmes do |t|
      t.string :name
      t.string :category
      t.string :branch
      t.datetime :start
      t.float :duration
      t.text :summary
      t.text :description
      t.text :rules
      t.string :gold_sponsor
      t.string :silver_sponsor
      t.string :bronze_sponsor

      t.timestamps null: false
    end
  end
end
