class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :college, :text
    add_column :users, :contact, :string
  end
end
