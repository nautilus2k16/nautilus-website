class AddColumnsToProgrammes < ActiveRecord::Migration
  def change
    add_column :programmes, :first_prize, :string
    add_column :programmes, :second_prize, :string
    add_column :programmes, :third_prize, :string
    add_column :programmes, :registerable, :boolean
    add_column :programmes, :prize, :boolean
    add_column :programmes, :team, :boolean
  end
end
