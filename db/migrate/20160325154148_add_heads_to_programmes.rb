class AddHeadsToProgrammes < ActiveRecord::Migration
  def change
    add_column :programmes, :head1, :string
    add_column :programmes, :number1, :string
    add_column :programmes, :head2, :string
    add_column :programmes, :number2, :string
  end
end
