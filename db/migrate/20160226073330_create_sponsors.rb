class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors do |t|
      t.string :name
      t.text :about
      t.float :amount
      t.string :link
      t.references :programme, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
