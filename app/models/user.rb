require 'securerandom'
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # attr_accessible :email, :contact, :college
  attr_protected
  has_many :registrations
  has_many :registered_events,through: :registrations, source: :programme

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable 
         # :validatable

  class << self
   def omniauth(auth_hash)
    user = find_or_create_by(uid: auth_hash['uid'], provider: auth_hash['provider'])
    user.name = auth_hash['info']['name']

    # auth_hash['info']['email']
    # "name#{rand(100..1000)}@random.com"
    user.password = SecureRandom.urlsafe_base64
    user.image = auth_hash['info']['image']
    user.token= auth_hash['credentials']['token']
    user.expires_at = auth_hash['credentials']['expires_at']
    user.save!
    user
   end
  end
end
