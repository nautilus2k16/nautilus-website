class Programme < ActiveRecord::Base
	has_many :sponsors
	has_many :registrations
  has_many :registered_users, through: :registrations, source: :user
end
