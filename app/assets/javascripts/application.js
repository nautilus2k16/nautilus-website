// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require_tree .
//= require nprogress
//= require nprogress-turbolinks
//= require jquery.form-validator
//= require turbolinks

NProgress.configure({
  showSpinner: false,
  ease: 'ease',
  speed: 500,
  trickle: false
});


$(document).ready(function() {
      //  $("body").css("display", "none");
      //  $("body").fadeIn(1000);
       $.validate();
       $(".button-collapse").sideNav();
       $('.parallax').parallax();




});
