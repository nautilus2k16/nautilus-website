class SponsorsController < ApplicationController
  def create
    @programme = Programme.find(params[:programme_id])
    @sponsor = @programme.sponsors.create(sponsor_params)
    redirect_to programme_path(@programme)
  end
 
  private
    def sponsor_params
      params.require(:sponsor).permit(:name, :about, :amount, :link)
    end
end
