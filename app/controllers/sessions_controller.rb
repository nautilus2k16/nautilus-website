class SessionsController < ApplicationController
  def create
    # @user = User.new
    @user = User.omniauth(request.env['omniauth.auth'])
    sign_in @user
    # redirect_to root_url
    redirect_to '/info/'
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end

  def info
  end

  def add_user

    # params.permit!
    # current_user = params.require(:user).permit(:email, :college, :contact)
    current_user.email = params[:user][:email]
    current_user.college = params[:user][:college]
    current_user.contact = params[:user][:contact]
    current_user.save
    # if @user.save
    #   flash[:notice] = "You signed up successfully"
    #   flash[:color]= "valid"
    # else
    #   flash[:notice] = "Form is invalid"
    #   flash[:color]= "invalid"
    # end
    redirect_to root_url
  end

  def auth_failure
    redirect_to root_path
  end
end
