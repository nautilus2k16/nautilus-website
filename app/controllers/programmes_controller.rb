class ProgrammesController < ApplicationController
  before_action :set_programme, only: [:show, :edit, :update, :destroy]
  require 'json'
  # GET /programmes
  # GET /programmes.json
  def home
    @color = "blue darken-3"
    @color1 = "blue darken-4"
  end

  def result
    @color = "blue darken-2"
    @color1 = "blue darken-3"

  end
  def schedule
    @color = "cyan darken-2"
    @color1 = "cyan darken-3"

  end
  def about
    @color = "red darken-3"
    @color1 = "red darken-4"

  end
  def index
    @color = "blue-grey darken-3"
    @color1 = "blue-grey darken-4"

    @programmes = Programme.all
  end
  def credit
    render :layout => "creditlayout"


  end
  # GET /programmes/1
  # GET /programmes/1.json
  def show
    @color = "green"
    @color1 = "green darken-1"
    @event_register=Programme.find(params[:id])
    if current_user!=nil
      @register=@event_register.registrations.find_by(user_id:current_user.id)
    end

  end

  # GET /programmes/new
  def new
    @color = "deep-purple darken-4"
    @programme = Programme.new
  end

  # GET /programmes/1/edit
  def edit
  end

  # POST /programmes
  # POST /programmes.json
  def create
    @color = "deep-purple darken-4"
    @programme = Programme.new(programme_params)

    respond_to do |format|
      if @programme.save
        format.html { redirect_to @programme, notice: 'Programme was successfully created.' }
        format.json { render :show, status: :created, location: @programme }
      else
        format.html { render :new }
        format.json { render json: @programme.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /programmes/1
  # PATCH/PUT /programmes/1.json
  def update
    respond_to do |format|
      if @programme.update(programme_params)
        format.html { redirect_to @programme, notice: 'Programme was successfully updated.' }
        format.json { render :show, status: :ok, location: @programme }
      else
        format.html { render :edit }
        format.json { render json: @programme.errors, status: :unprocessable_entity }
      end
    end
  end

  def api_events
    @programmes = Programme.all.as_json
    @programmes.each do |programme|
       programme = programme.map{|k,v| programme[k.to_s] = v.to_s}
    end
    @programmes = @programmes.to_json
    respond_to do |format|
       format.json {render :text => @programmes}
    end
  end

  def api_category
    @programme = Programme.where(category: params[:category]).as_json
    @programme.map{|k,v| @programme[k.to_s] = v.to_s}
    @programme = @programme.to_json

    respond_to do |format|
      format.json {render :text => @programme}
    end
  end

  def api_id
    # @programme = Programme.where(name: params[:name])
    # programme_id = @programme.pluck(:id)
    programme_id = params[:id]

      # if params[:id]
      #  @programme_sponsors_joined = Programme.includes(:sponsors).find(params[:id]).to_json(include: :sponsors)
      # end
    @programme = Programme.find(programme_id)
    @programme=@programme.as_json
    # Programme.joins(:sponsors).select("programmes.*", "sponsors.name as sponsor_name", "sponsors.about", "sponsors.link", "sponsors.amount").where(:programmes => {:id => programme_id}).to_json
    @programme.map{|k,v| @programme[k.to_s] = v.to_s}
    @programme = @programme.to_json

    respond_to do |format|
      format.json {render :text => @programme}
    end
  end

  def api_sponsors
    @sponsors = Sponsor.all.as_json
    @sponsors.each do |sponsor|
       sponsor = sponsor.map{|k,v| sponsor[k.to_s] = v.to_s}
    end
    @sponsors = @sponsors.to_json
    respond_to do |format|
       format.json {render :text => @sponsors}
    end
  end

  # DELETE /programmes/1
  # DELETE /programmes/1.json
  def destroy
    @programme.destroy
    respond_to do |format|
      format.html { redirect_to programmes_url, notice: 'Programme was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_programme
      @programme = Programme.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def programme_params
      params.require(:programme).permit(:name, :category, :branch, :start, :duration, :summary, :description, :rules, :gold_sponsor, :silver_sponsor, :bronze_sponsor,:event_image)
    end
end
