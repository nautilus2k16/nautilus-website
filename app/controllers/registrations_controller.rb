class RegistrationsController < ApplicationController
	def register
		@event=Programme.find(params[:id])
		@register_event=@event.registrations.new
		@register_event.user_id=current_user.id
		@register_event.save
		if current_user!=nil
    	@register=@event.registrations.find_by(user_id:current_user.id)
  	end
		respond_to do |format|
			format.html{redirect_to programme_path(@event)}
    	format.js  { render 'registrations/register.js.erb'}
  	end

	end

	def delete
		@registered_events=current_user.registered_events
		event=Programme.find(params[:id])
		delete_event=event.registrations.find_by(user_id:current_user.id)
		delete_event.delete
		respond_to do |format|
			format.js{render 'registrations/unregister.js.erb'}
		end
	end
end
