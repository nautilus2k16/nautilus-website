class ProfileController < ApplicationController
before_action :authenticate_user!

def show

  @qr = RQRCode::QRCode.new("#{current_user.email}", :size => 4, :level => :h)

  @registered_events=current_user.registered_events
end

end
