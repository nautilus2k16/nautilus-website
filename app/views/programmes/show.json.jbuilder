json.extract! @programme, :id, :name, :category, :branch, :start, :duration, :summary, :description, :rules, :gold_sponsor, :silver_sponsor, :bronze_sponsor, :created_at, :updated_at
