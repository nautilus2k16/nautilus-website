json.sponsors @sponsors do |sponsor|
    json.(sponsor, :id, :name, :about, :amount, :link, :created_at, :updated_at)
end
