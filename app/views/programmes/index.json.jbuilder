json.array!(@programmes) do |programme|
  json.extract! programme, :id, :name, :category, :branch, :start, :duration, :summary, :description, :rules, :gold_sponsor, :silver_sponsor, :bronze_sponsor
  json.url programme_url(programme, format: :json)
end
