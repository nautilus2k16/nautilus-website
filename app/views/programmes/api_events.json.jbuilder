json.programmes @programmes do |programme|
  json.(programme, "id", :name, :branch, :start, :duration, :summary, :description, :rules, :gold_sponsor, :silver_sponsor, :bronze_sponsor, :created_at, :updated_at)
end
