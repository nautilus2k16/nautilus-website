json.(@programme, :id, :name, :branch, :start, :duration, :summary, :description, :rules, :gold_sponsor, :silver_sponsor, :bronze_sponsor, :created_at, :updated_at)
 
json.sponsors @programme.sponsors do |sponsor|
    json.(sponsor, :id, :name, :about, :amount, :link, :created_at, :updated_at)
end
